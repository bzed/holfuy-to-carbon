#!/usr/bin/env python3

import yaml
import time
import graphyte
from lxml import etree
import sys
import requests

CONFIG = {}


def update_station(station_id, station_name):
    url = f"http://holfuy.hu/en/takeit/xml/dezso/data.php?station={station_id}&cnt=1"
    r = requests.get(url)
    xml_data = r.content
    weather_data = etree.fromstring(xml_data)[0]
    weather_data = {i.tag: i.text for i in weather_data.iterchildren()}

    measurement_time = time.time() - int(weather_data["sec_back"])

    print("Updating {}/{}: {}".format(station_id, station_name, str(weather_data)))

    measurements = list(weather_data.keys())
    measurements.remove("date")
    measurements.remove("time")
    measurements.remove("sec_back")

    for i in measurements:
        _v = float(weather_data[i])
        metric = f"{station_name}.{i}.value"
        graphyte.send(
            metric,
            _v,
            timestamp=measurement_time,
        )


def main():
    keep_running = True
    graphyte.init(**CONFIG["carbon"])

    while keep_running:
        now = time.time()

        for _id, _name in CONFIG["stations"].items():
            try:
                update_station(_id, _name)
            except KeyboardInterrupt:
                print("Received Keyboard interrupt, exiting")
                keep_running = False
                break
            except Exception as e:
                print("Update of station {}/{} failed: {}".format(_id, _name, e))

        next_run = now + 120
        sleep_time = next_run - time.time()
        if sleep_time > 0:
            time.sleep(sleep_time)


if __name__ == "__main__":

    print("Starting holfuy-to-carbon!")
    if len(sys.argv) < 2:
        print("Usage: {} <configfile>".format(sys.argv[0]))
        sys.exit(1)

    with open(sys.argv[1], "r") as config_file:
        CONFIG = yaml.safe_load(config_file)
    try:
        main()
    except KeyboardInterrupt:
        print("Received Keyboard interrupt, exiting")
